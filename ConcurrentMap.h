#pragma once

#include <iostream>
#include <map>
#include <memory>
#include <mutex>

template <typename Key, typename T>
class ConcurrentMap
{
private:
	typedef std::pair<std::mutex, T> data_t;
	typedef std::map<Key, std::shared_ptr<data_t>> map_t;

	map_t map;
	mutable std::mutex mutex;

public:
	class Accessor
	{
	private:
		std::unique_lock<std::mutex> lock;

	public:
		const Key &key;
		T &value;

		Accessor(const Key &key, data_t &data):
			lock(data.first), key(key), value(data.second)
		{}

		Accessor(const Accessor&) = delete;
		Accessor& operator= (const Accessor&) = delete;

		Accessor(Accessor&&) = default;
	};

	ConcurrentMap() = default;
	virtual ~ConcurrentMap() = default;

	Accessor get(const Key &key)
	{
		std::lock_guard<std::mutex> lock(this->mutex);

		auto &result = this->map[key];
		if (!result)
			result = std::make_shared<data_t>();

		return {key, *result};
	}

	void set(const Key &key, const T &value)
	{
		auto element = this->get(key);
		element.value = value;
	}

	void print_all() const
	{
		std::lock_guard<std::mutex> lock(this->mutex);
		for (auto &el : this->map)
		{
			std::lock_guard<std::mutex> inner_lock(el.second->first);
			std::cout << el.first << ": " << el.second->second << std::endl;
		}
	}
};
