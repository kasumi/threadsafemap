#pragma once

#include <initializer_list>
#include <iostream>
#include <mutex>
#include <map>
#include <vector>

template <typename Key, typename T>
class ThreadSafeMap
{
private:
	typedef std::map<Key, T> map_t;

	map_t map;
	mutable std::mutex mutex;

public:
	ThreadSafeMap() {}
	virtual ~ThreadSafeMap() {}

	T get(const Key &key)
	{
		std::lock_guard<std::mutex> lock(this->mutex);
		return this->map[key];
	}

	std::vector<T> get(const std::initializer_list<Key> &&keys)
	{
		std::lock_guard<std::mutex> lock(this->mutex);

		std::vector<T> result;
		result.reserve(keys.size());

		for (auto &key : keys)
			result.push_back(this->map[key]);

		return result;
	}

	void set(const Key &key, const T &value)
	{
		std::lock_guard<std::mutex> lock(this->mutex);
		this->map[key] = value;
	}

	void print_all() const
	{
		std::lock_guard<std::mutex> lock(this->mutex);
		for (auto &el : this->map)
			std::cout << el.first << ": " << el.second << std::endl;
	}
};
