#include "ThreadSafeMap.h"
#include "ConcurrentMap.h"

#include <array>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

static const uint8_t THREADS_COUNT = 100;

typedef std::pair<std::string, std::string> map_pair_t;
static const std::vector<map_pair_t> DICTIONARY = {
	{"apple", "green"},
	{"banana", "yellow"},
	{"blueberry", "blue"},
	{"carrot", "orange"},
	{"broccolo", "green"},
	{"mango", "orange"},
	{"tomato", "red"},
	{"grapes", "red-purple"},
	{"papaya", "orange-yellow"},
	{"avocado", "yellow-green"},
};

typedef ThreadSafeMap<std::string, std::string> thread_safe_map_t;
typedef ConcurrentMap<std::string, std::string> concurrent_map_t;

template <typename T>
void write_random(T *map)
{
	size_t index = random() % DICTIONARY.size();
	map->set(DICTIONARY[index].first, DICTIONARY[index].second);
}

void read_random(thread_safe_map_t *map)
{
	size_t index = random() % DICTIONARY.size();
	map->get(DICTIONARY[index].first);
}

void change_random(concurrent_map_t *map)
{
	size_t index = random() % DICTIONARY.size();
	auto accessor = map->get(DICTIONARY[index].first);
	accessor.value += " modified";
}

int main()
{
	thread_safe_map_t thread_safe_map;
	concurrent_map_t concurrent_map;
	std::array<std::thread, THREADS_COUNT> threads;

	srand(time(nullptr));

	for (auto &thread : threads)
	{
		uint8_t method = random() % 4;

		switch (method) {
		case 0:
			thread = std::thread(read_random, &thread_safe_map);
			break;
		case 1:
			thread = std::thread(change_random, &concurrent_map);
			break;
		case 2:
			thread = std::thread(write_random<thread_safe_map_t>, &thread_safe_map);
			break;
		case 3:
			thread = std::thread(write_random<concurrent_map_t>, &concurrent_map);
			break;
		default:
			break;
		}
	}

	std::cout << "========= Access to multiple values in thread-safe map" << std::endl;
	auto values = thread_safe_map.get({"apple", "mango", "carrot"});
	for (auto &el : values)
		std::cout << el << ", ";
	std::cout << std::endl << std::endl;

	for (auto &thread : threads)
		thread.join();

	std::cout << "========= Print thread-safe map after all" << std::endl;
	thread_safe_map.print_all();
	std::cout << std::endl << std::endl;

	std::cout << "========= Print concurent map after all" << std::endl;
	concurrent_map.print_all();
	std::cout << std::endl << std::endl;

	return 0;
}

